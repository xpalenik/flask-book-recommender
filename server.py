import pandas as pd
from flask import Flask, request, redirect, url_for, render_template
from flask_table import Table, Col

app = Flask(__name__)


class ItemTable(Table):
    title = Col('title')
    similarity = Col('similarity')
    count = Col('count')
    normalized_count = Col('normalized_count')
    normalized_similarity = Col('normalized_similarity')
    adjusted_similarity = Col('adjusted_similarity')


class Book(object):
    def __init__(self, title, similarity, count, normalized_count, normalized_similarity, adjusted_similarity):
        self.title = title
        self.similarity = similarity
        self.count = count
        self.normalized_count = normalized_count
        self.normalized_similarity = normalized_similarity
        self.adjusted_similarity = adjusted_similarity


def normalize(df, column):
    return (df[column] - df[column].min()) / (df[column].max() - df[column].min())


def get_recommendations():
    results = pd.read_csv('result.csv')
    results['normalized_count'] = normalize(results, 'count')
    results['normalized_similarity'] = normalize(results, 'similarity')

    items = []
    for result in results.values:
        items.append(
            Book(result[0], result[1], result[2], result[3], result[4], 0.6 * result[3] + 0.4 * result[4])
        )

    items.sort(key=lambda x: x.adjusted_similarity, reverse=True)

    return ItemTable(
        items,
        no_items='There is nothing',
        html_attrs={'style': "margin-left: auto; margin-right: auto;"}
    )


def get_html(where):
    if where == 'before':
        return '<!doctype html>' \
               '<html lang=en>' \
               '<head>' \
               '<meta charset=utf-8>' \
               '<title>blah</title>' \
               '</head>' \
               '<body>'

    if where == 'after':
        return '</body>' \
               '</html>'


@app.route('/predict/<book_title>')
def predict(book_title):
    table = get_recommendations()
    table.border = True
    return get_html('before') + \
           '<h1 style ="text-align: center">' + 'Recommending books similar to: ' + book_title + '</h3>' + \
           table.__html__() + \
           get_html('after')


@app.route("/hi", methods=["POST"])
def process_image():
    book_title = request.form['book_title']
    return redirect(url_for('predict', book_title=book_title))


@app.route('/')
def index():
    return render_template('index.html')


if __name__ == "__main__":
    app.run(debug=True)
